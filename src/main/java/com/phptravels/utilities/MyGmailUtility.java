package com.phptravels.utilities;

/**
 * This file i made it to use the method i need from 
 *  EmailUtils.java file by @Angie Jones 
 * Here is the link :
 * http://angiejones.tech/test-automation-to-verify-email/
 * */
import java.util.Properties;

import javax.mail.Message;

public class MyGmailUtility {
	private Properties config = null;
	private EmailUtils emailUtils;

	public MyGmailUtility(Properties config) {
		this.config = config;
	}

	//this method create a new random Gmail address to be used in test 
	public String getRandomGmailAddress() {
		String BaseGmailAddress = config.getProperty("BaseGmail_Addr");
		String currentGmailAddress = config.getProperty("RandomGeneratedGmail_Addr");
		String newGmailAddress = null;

		// Check if the Gmail was used for the first time
		if (currentGmailAddress.equalsIgnoreCase(BaseGmailAddress)) {
			String[] stringSplitter = BaseGmailAddress.split("@");
			newGmailAddress = stringSplitter[0] + "+TC1110" + stringSplitter[1];
		} else {
			/**********************************************************************
			 * All i want to do is for example to change this email
			 * dummytestacco7+TC1110@gmail.com to dummytestacco7+TC1111@gmail.com This is
			 * can be done be: 1. Split the currentGmailAddress by (+) 2. Take the right
			 * half 3. Split this right half by (@) 4. Take the left half and clear the TC
			 * 5. Parse the String ("1110") to int 6. Then increase it be 1 (1111) 7. Then
			 * assemble those back together
			 **********************************************************************/

			String[] stringSplitter = currentGmailAddress.split("\\+");
			int num = Integer.parseInt((stringSplitter[1].split("@"))[0].substring(2));
			num++;
			newGmailAddress = stringSplitter[0] + "+TC" + num + "@gmail.com";

		}
		// Update the Gmail
		config.setProperty("RandomGeneratedGmail_Addr", newGmailAddress);
		return newGmailAddress;
	}
	
	//this method is to check if there is a link in an email or not 
	public boolean isVerificationLinkExist(String MessageTitle, String verificationLinkName)  {
		Message email = null;
		try {
			connectToMyGmail();
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			email = emailUtils.getMessagesBySubject(MessageTitle, false, 5)[0];
			try {
				emailUtils.getUrlsFromMessage(email, verificationLinkName).get(0);
			} catch (IndexOutOfBoundsException e) {
				return false;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return true;
	}

	private void connectToMyGmail() throws Exception {
		emailUtils = new EmailUtils(config.getProperty("BaseGmail_Addr"), config.getProperty("BaseGmail_Pass"),
				"smtp.gmail.com", EmailUtils.EmailFolder.INBOX);
	}



}
