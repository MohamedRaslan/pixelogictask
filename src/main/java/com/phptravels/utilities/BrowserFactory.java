package com.phptravels.utilities;

import java.util.Properties;

import org.apache.log4j.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {

	private WebDriver driver = null;
	private Logger log = null;
	private Properties config = null;

	public BrowserFactory(Logger log, Properties config) {
		this.log = log;
		this.config = config;
	}

	public WebDriver getMyBrowser(String browserName) {
		// For Mozilla Firefox Browser
		if (browserName.equalsIgnoreCase("FireFox")) 
			return getFirefox(getWebdriverAbsolutePath(browserName));
		else 
			return getChrome(getWebdriverAbsolutePath(browserName));
		

	}

	// Initiate Chrome Webdriver
	private WebDriver getChrome(String absolutePathToWebDriver) {
		System.setProperty("webdriver.chrome.driver", absolutePathToWebDriver);
		driver = new ChromeDriver(); // Launch Google Chrome
		log.debug("Google Chrome Launched !!!");
		return driver;
	}

	// Initiate Firefox Webdriver
	private WebDriver getFirefox(String absolutePathToWebDriver) {
		System.setProperty("webdriver.gecko.driver", absolutePathToWebDriver);
		driver = new FirefoxDriver(); // Launch Mozilla Firefox
		log.debug("Mozilla Firefox Launched !!!");
		return driver;
	}

	// gets the location of the choosing Webdriver
	private String getWebdriverAbsolutePath(String browserName) {
		String relativePath = null;
		String AbsolutePath = null;

		// For Mozilla Firefox Browser
		if (browserName.equalsIgnoreCase("FireFox")) {
			// For Windows OS
			if (System.getProperty("os.name").contains("Windows")) {
				relativePath = "drivers\\Windows-64\\geckodriver.exe";
				log.debug("Firefox will Launch for Windows OS ...");
			}
			// For Mac OS
			else if (System.getProperty("os.name").contains("Mac")) {
				relativePath = "drivers\\Mac-64\\geckodriver";
				log.debug("Firefox will Launch for Mac OS ...");
			}
			// For Linux OS
			else if (System.getProperty("os.name").contains("Linux")) {
				relativePath = "drivers\\Linux-64\\geckodriver";
				log.debug("Firefox will Launch for Linux OS ...");
			}
			AbsolutePath = System.getProperty("user.dir") + config.getProperty("executableRelativePath") + relativePath;
			log.debug("The Absolute Path to the Webdriver is :: " + AbsolutePath );
			return AbsolutePath;
		}
		// For Google Chrome Browser
		else {
			// For Windows OS
			if (System.getProperty("os.name").contains("Windows")) {
				relativePath = "drivers\\Windows-64\\chromedriver.exe";
				log.debug("Google Chrome will Launch for Windows OS ...");
			}
			// For Mac OS
			else if (System.getProperty("os.name").contains("Mac")) {
				relativePath = "drivers\\Mac-64\\chromedriver";
				log.debug("Google Chrome will Launch for Mac OS ...");
			}
			// For linux OS
			else if (System.getProperty("os.name").contains("linux")) {
				relativePath = "drivers\\Linux-64\\chromedriver";
				log.debug("Google Chrome will Launch for Linux OS ...");
			}
			AbsolutePath = System.getProperty("user.dir") + config.getProperty("executableRelativePath") + relativePath;
			log.debug("The Absolute Path to the Webdriver is :: " + AbsolutePath );
			return AbsolutePath;
		}
	}

}
