package com.phptravels.pom;

import java.util.Arrays;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class HomePage extends PomBase {

	public HomePage(WebDriver driver,Logger log, Properties pomData, long waitTimeOutInSeconds  ) {
		super(driver,log, pomData, waitTimeOutInSeconds);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//div[ @class='dropdown dropdown-login dropdown-tab'] //a[ @id='dropdownCurrency']" )
	WebElement MyAccountBtn;
	
	@FindBy(xpath = "//a[@class='dropdown-item tr']" )
	WebElement SignUpBtn;
	
	@FindBy(xpath = "//div[@class='menu-horizontal-wrapper-02']" )
	WebElement SearchBoxDiv;
	
	public boolean isTheHomePage() {
		return  isThePage(Arrays.asList(MyAccountBtn,SearchBoxDiv), "HomePage_title");
	}
	public void openRegisterForm() {
		mylogger.debug("IN openRegisterForm Method ");
		wait.until(ExpectedConditions.visibilityOf(MyAccountBtn)).click();
		mylogger.debug("Clicking on MyAccountBtn");
		wait.until(ExpectedConditions.visibilityOf(SignUpBtn)).click();
		mylogger.debug("Clicking on SignUpBtn");
	}
	
	public String accountName() {
		mylogger.debug("IN accountName Method ");
		mylogger.debug("Return the Account Name");
		return wait.until(ExpectedConditions.visibilityOf(MyAccountBtn)).getText();
		
	}

}
