package com.phptravels.pom;

import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PomBase {

	protected WebDriver driver = null;
	protected WebDriverWait wait = null;
	protected Logger mylogger = null;
	protected Properties pomData = null;
	JavascriptExecutor executor = null;

	protected PomBase(WebDriver driver, Logger log, Properties pomData, long waitTimeOutInSeconds) {
		this.driver = driver;
		this.pomData = pomData;
		mylogger = log;
		wait = new WebDriverWait(driver, waitTimeOutInSeconds);
		executor = (JavascriptExecutor)driver;
	}

	protected boolean isThePage(List<WebElement> Elements, String Page_title) {
		if (driver.getTitle().equals(pomData.getProperty(Page_title))) {
			mylogger.debug("The Page Title is correct expected : " + pomData.getProperty(Page_title) + ", And the actual is : " +driver.getTitle() );
			try {
				List<WebElement> myElements = wait.until(ExpectedConditions.visibilityOfAllElements(Elements));
				for (WebElement anElement : myElements) {
					if (!anElement.isDisplayed()) {
						mylogger.debug("The " + anElement.getText() + " element is not present in this page");
						return false;
					}
				}
			} catch (NoSuchElementException e) {
				mylogger.error("There is no Such Element Exception \n", e);
				return false;
			} catch (TimeoutException e) {
				mylogger.error("There is an Time out Exception \n", e);
				return false;
			}

			return true;
		} else {
			mylogger.debug("The Page Title is not correct expected : " + pomData.getProperty(Page_title) + ", But the actual is : " +driver.getTitle() );
			return false;
		}
	}

}
