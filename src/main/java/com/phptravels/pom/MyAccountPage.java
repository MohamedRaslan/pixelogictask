package com.phptravels.pom;

import java.util.Arrays;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MyAccountPage extends PomBase {

	public MyAccountPage(WebDriver driver, Logger log, Properties pomData, long waitTimeOutInSeconds) {
		super(driver, log, pomData, waitTimeOutInSeconds);
		PageFactory.initElements(driver, this);
	}

	/************************************************************
	 * My Account Page Elements https://www.phptravels.net/account
	 ************************************************************/

	@FindBy(xpath = "//ul[@class=\"menu-vertical-01\"]//a[@href=\"#profile\"]")
	WebElement MyProfileTab;

	@FindBy(xpath = "//div[@class=\"col-md-4\"]//img")
	WebElement MyAccountImage;

	@FindBy(xpath = "//div[@class=\"col-md-8\"]//h3")
	WebElement MyAccountHiMsg;

	/****************************************************************************
	 * The Following elements are hidden,You need to press on My Profile tab to make
	 * them visible
	 *****************************************************************************/

	@FindBy(name = "firstname")
	WebElement MyProfile_FirstNameBox;

	@FindBy(name = "lastname")
	WebElement MyProfile_LastNameBox;

	@FindBy(name = "email")
	WebElement MyProfile_EmailBox;

	public boolean isMyAccountPage() {
		return isThePage(Arrays.asList(MyProfileTab, MyAccountImage, MyAccountHiMsg), "LoginPage_title");
	}

	public boolean isMyProfile(String firstName, String lastName, String email) {
		mylogger.debug("IN isMyProfile Method ");
		try {
			if (wait.until(ExpectedConditions.visibilityOf(MyAccountHiMsg)).getText()
					.equals("Hi, " + firstName + " " + lastName)) {
				mylogger.debug("The MyAccountHiMsg was correct expected : " + "Hi, " + firstName + " " + lastName
						+ ", And the actual is : " + MyAccountHiMsg.getText());
				wait.until(ExpectedConditions.visibilityOf(MyProfileTab)).click();

				if (!wait.until(ExpectedConditions.visibilityOf(MyProfile_FirstNameBox)).getAttribute("value").equals(firstName)) {
					mylogger.debug("The MyProfile_FirstName is not correct expected : " + firstName
							+ ", But the actual is : " + MyProfile_FirstNameBox.getAttribute("value"));
					return false;
				} else if (!wait.until(ExpectedConditions.visibilityOf(MyProfile_LastNameBox)).getAttribute("value")
						.equals(lastName)) {
					mylogger.debug("The MyProfile_LastNameBox is not correct expected : " + lastName
							+ ", But the actual is : " + MyProfile_LastNameBox.getAttribute("value"));
					return false;
				} else if (!wait.until(ExpectedConditions.visibilityOf(MyProfile_EmailBox)).getAttribute("value").equals(email)) {
					mylogger.debug("The MyProfile_EmailBox is not correct expected : " + email
							+ ", But the actual is : " + MyProfile_EmailBox.getAttribute("value"));
					return false;
				}
			} else {
				mylogger.debug("This is Not MyProfile expected : " + "Hi, " + firstName + " " + lastName
						+ ", But the actual is : " + MyAccountHiMsg.getText());
				return false;
			}
		} catch (TimeoutException e) {
			mylogger.error("There is an Time out Exception \n", e);
			return false;
		}

		mylogger.debug("This is My Profile Page ");
		return true;
	}

}
