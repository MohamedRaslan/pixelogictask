package com.phptravels.pom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends PomBase {

	public LoginPage(WebDriver driver, Logger log,Properties pomData , long waitTimeOutInSeconds) {
		super(driver, log, pomData, waitTimeOutInSeconds);
		PageFactory.initElements(driver, this);
	}

	/****************************
	 * Registration Form Elements https://www.phptravels.net/register
	 ****************************/

	@FindBy(name = "firstname")
	WebElement firstNameBox;

	@FindBy(name = "lastname")
	WebElement lastNameBox;

	@FindBy(name = "phone")
	WebElement phoneBox;

	@FindBy(name = "email")
	WebElement emailBox;

	@FindBy(name = "password")
	WebElement passwordBox;

	@FindBy(name = "confirmpassword")
	WebElement confirmpasswordBox;

	@FindBy(xpath = "//button[@type='submit' and @class='signupbtn btn_full btn btn-success btn-block btn-lg']")
	WebElement loginSubmitBtn;

	/****************************
	 * Alert Message Appears when data in any field is invalid
	 ****************************/

	// Alert messages in the registration form
	@FindBy(xpath = "//div[@class='alert alert-danger']")
	List<WebElement> alertMessages;

	public void RegisterNewAccount(String firstName, String lastName, String phone, String email, String Password,
			String confirmpassword, boolean submitTheForm) {

		mylogger.debug("IN RegisterNewAccount Method ");

		mylogger.debug("Clearing all the input fields");
		wait.until(ExpectedConditions.visibilityOf(firstNameBox)).clear();
		wait.until(ExpectedConditions.visibilityOf(lastNameBox)).clear();
		wait.until(ExpectedConditions.visibilityOf(phoneBox)).clear();
		wait.until(ExpectedConditions.visibilityOf(emailBox)).clear();
		wait.until(ExpectedConditions.visibilityOf(passwordBox)).clear();
		wait.until(ExpectedConditions.visibilityOf(confirmpasswordBox)).clear();

		if (firstName != "") {
			firstNameBox.sendKeys(firstName);
			mylogger.debug("Sending:: " + firstName + " To the firstNameBox");
		}
		if (lastName != "") {
			lastNameBox.sendKeys(lastName);
			mylogger.debug("Sending:: " + lastName + " To the lastNameBox");
		}

		if (phone != "") {
			phoneBox.sendKeys(phone);
			mylogger.debug("Sending:: " + phone + " To the phoneBox");
		}
		if (email != "") {
			emailBox.sendKeys(email);
			mylogger.debug("Sending:: " + email + " To the emailBox");
		}

		if (Password != "") {
			passwordBox.sendKeys(Password);
			mylogger.debug("Sending:: " + Password + " To the passwordBox");
		}
		if (confirmpassword != "") {
			confirmpasswordBox.sendKeys(confirmpassword);
			mylogger.debug("Sending:: " + confirmpassword + " To the confirmpasswordBox");
		}

		if (submitTheForm) {
			//wait.until(ExpectedConditions.visibilityOf(loginSubmitBtn)).click();
			executor.executeScript("arguments[0].click();", loginSubmitBtn);
			mylogger.debug("Clicking on loginSubmitBtn");
		}
	}

	public boolean isTheLoginPage() {
		return isThePage(Arrays.asList(firstNameBox, lastNameBox, phoneBox, emailBox, passwordBox, confirmpasswordBox,
				loginSubmitBtn), "LoginPage_title");
	}

	public List<String> getAlertMessages() {
		mylogger.debug("IN getAlertMessages Method ");
		List<String> dataMessages = new ArrayList<String>();
		StringBuilder theMessage = new StringBuilder();
		for (WebElement alertMessage : wait.until(ExpectedConditions.visibilityOfAllElements(alertMessages))) {
			dataMessages.add(alertMessage.getText());
			theMessage.append(alertMessage.getText());
		}
		mylogger.debug("Return the following Alert Messages ::" + theMessage);
		return dataMessages;

	}
	
	public String getAlertMessagesAsString() {
		StringBuilder theMessage = new StringBuilder();
		for (String alertMessage : getAlertMessages()) {
			theMessage.append(alertMessage);
		}
		return theMessage.toString();
	}

	public boolean isAlertMessagesAsExpected(String expectedData) {
		mylogger.debug("IN isAlertMessagesAsExpected Method ");
		List<String> ActualData = getAlertMessages();
		String [] ExpectedData = expectedData.split("\\r?\\n");
	
		for(int i=0 ; i < ActualData.size() ; i++) {
			mylogger.debug("The Actual Data is : [" + ActualData.get(i) + "] , and the Expected is : [" + ExpectedData[i] + "]");
			if(!ActualData.get(i).equalsIgnoreCase(ExpectedData[i])) {
				return false;
			}
		}
		
		return true;
	}

	
}
