package com.phptravels.TestUtilities;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;

import com.phptravels.base.TestBase;

public class TestUtil extends TestBase {
	public static String screenshotPath = System.getProperty("user.dir") + MyConstants.screenshot_FolderPath;
	public static String screenshotName;

	public static void captureScreenshot(WebDriver myDriver) throws IOException {

		File scrFile = ((TakesScreenshot) myDriver).getScreenshotAs(OutputType.FILE);

		Date d = new Date();
		screenshotName = "screenshot_" + d.toString().replace(":", "-").replace(" ", "_") + ".jpg";

		FileUtils.copyFile(scrFile, new File(screenshotPath + screenshotName));
	}

	@DataProvider(name = "DAtA_PROVIDER")
	public Object[][] getData(Method javaMethodRef) {

		String sheetName = javaMethodRef.getName();
		int rows = excel.getRowCount(sheetName);
		int cols = excel.getColumnCount(sheetName);

		// With Hashtable
		Object[][] data = new Object[rows - 1][1];
		Hashtable<String, String> table = null;

		for (int rowNum = 2; rowNum <= rows; rowNum++) { // 2

			table = new Hashtable<String, String>();
			for (int colNum = 0; colNum < cols; colNum++) {
				// data[0][0]
				table.put(excel.getCellData(sheetName, colNum, 1), excel.getCellData(sheetName, colNum, rowNum));
				data[rowNum - 2][0] = table;
			}
		}
		return data;
	}

	public static boolean isTestRunnable(String testCaseName, String sheetNameTest, ExcelReader excel) {

		String sheetName;
		if (sheetNameTest == MyConstants.testSuiteToRun)
			sheetName = config.getProperty(sheetNameTest);
		else
			sheetName = sheetNameTest;

		int rows = excel.getRowCount(sheetName);

		for (int rNum = 2; rNum <= rows; rNum++) {

			String testCase = excel.getCellData(sheetName, "TCID", rNum);

			if (testCase.equalsIgnoreCase(testCaseName)) {

				String runmode = excel.getCellData(sheetName, "runMode", rNum);

				if (runmode.equalsIgnoreCase("Y"))
					return true;
				else
					return false;
			}
		}
		return false;
	}

}
