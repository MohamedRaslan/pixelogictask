package com.phptravels.TestUtilities;

public class MyConstants {
	public static final  String Config_FilePath="\\src\\test\\resources\\properties\\Config.properties";
	public static final  String pageDATA_FilePath="\\src\\test\\resources\\properties\\pageDATA.properties";
	public static final  String emailDATA_FilePath="\\src\\test\\resources\\properties\\emailDATA.properties";

	public static final  String testdata_FilePath="\\src\\test\\resources\\excel\\testdata.xlsx";
	public static final  String screenshot_FolderPath="\\target\\surefire-reports\\html\\screenshots\\";
	public static final  String ReportsConfig_FolderPath="\\src\\test\\resources\\reportConfig\\ReportsConfig.xml";
	public static final  String ExtentRepo_FolderPath="\\target\\surefire-reports\\html\\ExtentReports\\ExtentRepo.html";

	
	public static final  String ValidGmailKeyWord= "validGmail";
	public static final  String sameValidGmailKeyWord= "sameValidGmail";
	public static final  String verificationLinkKeyWord ="VerificationLink";
	public static final  String MessageTitleKeyWord= "Signed Up successfully";
	public static final  String verificationLinkNameKeyWord= "No Link Exist";
	
	
	public static final  String testSuiteToRun= "testSuiteName";
	
}
