package com.phptravels.TestUtilities;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class ExtentRepoManager {
	
	private static ExtentReports extent ;
	private static String ExtentRepoPath=System.getProperty("user.dir") + MyConstants.ExtentRepo_FolderPath;
	private static String ExtentRepoCongigPath=System.getProperty("user.dir") + MyConstants.ReportsConfig_FolderPath ;
	public static ExtentReports getInstance() {
		
		
		if( extent == null) {
			 ExtentHtmlReporter HTMLReporter = new ExtentHtmlReporter(ExtentRepoPath);
			 HTMLReporter.loadXMLConfig(ExtentRepoCongigPath);
			 extent = new ExtentReports();
			 
			 /********************************************************************************************
			 HTMLReporter.config().setAutoCreateRelativePathMedia(true);
			 HTMLReporter.config().setCSS("css-string");
			 HTMLReporter.config().setDocumentTitle("Reports For A Data Driven Framework");
			 HTMLReporter.config().setEncoding("utf-8");
			 HTMLReporter.config().setJS("js-string");
			 HTMLReporter.config().setProtocol(Protocol.HTTPS);
			 HTMLReporter.config().setReportName("Data Driven Framework Report For WebsiteExample.com");
			 HTMLReporter.config().setTheme(Theme.DARK);
			 HTMLReporter.config().setTimeStampFormat("MMM dd, yyyy HH:mm:ss");
			**********************************************************************************************/
			 extent.attachReporter(HTMLReporter);
			 
			 extent.setSystemInfo("HostName", "LocalHost");
			 extent.setSystemInfo("OS", "Windoes 10");
			 extent.setSystemInfo("Tester Name", "M0hamed R@slan");
			 extent.setSystemInfo("Browser", "Google Chrome");
	

		}
		
		
		return extent ;
		
	}
	

}
