package com.phptravels.testCases;

import static org.testng.Assert.assertTrue;

import java.util.Hashtable;

import org.testng.annotations.Test;

import com.phptravels.TestUtilities.MyConstants;
import com.phptravels.TestUtilities.TestUtil;
import com.phptravels.base.TestBase;

public class VerifyPassAndConfirmPass extends TestBase {

	@Test(dataProviderClass = TestUtil.class, dataProvider = "DAtA_PROVIDER")
	public void verifyPassAndConfirmPass(Hashtable<String, String> data) {
		homePage.openRegisterForm();

		loginPage.RegisterNewAccount(data.get("firstName"), data.get("lastName"), data.get("phone"),
				getGmailAccount(data.get("email")), data.get("Password"), data.get("confirmpassword"),
				data.get("submitTheForm").equalsIgnoreCase("true"));

		if (data.get("expectedData").equalsIgnoreCase(MyConstants.verificationLinkKeyWord)) {
			assertTrue(myGmailUtility.isVerificationLinkExist(MyConstants.MessageTitleKeyWord,
					MyConstants.verificationLinkNameKeyWord), "No Verification Link Found in the Gmail");
		} else
			assertTrue(loginPage.isAlertMessagesAsExpected(data.get("expectedData")),
					"Alert Messages didn't match with the expected alert messages");
	}
}
