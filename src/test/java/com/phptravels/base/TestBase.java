package com.phptravels.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.phptravels.TestUtilities.ExcelReader;
import com.phptravels.TestUtilities.ExtentRepoManager;
import com.phptravels.TestUtilities.MyConstants;
import com.phptravels.pom.HomePage;
import com.phptravels.pom.LoginPage;
import com.phptravels.pom.MyAccountPage;
import com.phptravels.utilities.BrowserFactory;
import com.phptravels.utilities.MyGmailUtility;

public class TestBase {

	public static WebDriver driver;
	public static Properties config = new Properties();
	public static Properties pageDATA = new Properties();
	public static Properties emailDATA = new Properties();
	public static FileInputStream fis;
	public static FileOutputStream fos;
	public static Logger log = Logger.getLogger("devpinoyLogger");
	public static MyGmailUtility myGmailUtility;
	public static ExcelReader excel = new ExcelReader(System.getProperty("user.dir") + MyConstants.testdata_FilePath);

	public ExtentReports Repo = ExtentRepoManager.getInstance();
	public static ExtentTest RepoTest;
	public static ExtentTest RepoNode;

	public HomePage homePage = null;
	public LoginPage loginPage = null;
	public MyAccountPage myAccountPage = null;
	public String WebsiteTestingURL = null;
	public ITestContext context; // creating a ITestContext variable

	@Parameters("MY_Browser")
	@BeforeClass
	public void setUp(String myBrowser, ITestContext iTestContext) throws MalformedURLException {
		log.debug("Test Execution Starts !!!");
		if (driver == null) {

			// Downloading the pageDATA File
			try {
				fis = new FileInputStream(System.getProperty("user.dir") + MyConstants.pageDATA_FilePath);
				try {
					pageDATA.load(fis);
					log.debug("pageDATA file loaded !!!");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}

			// Downloading the Config File
			try {
				fis = new FileInputStream(System.getProperty("user.dir") + MyConstants.Config_FilePath);
				try {
					config.load(fis);
					log.debug("Config file loaded !!!");

				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			
			// Downloading the emaemailDATAil File
			try {
				fis = new FileInputStream(System.getProperty("user.dir") + MyConstants.emailDATA_FilePath);
				try {
					emailDATA.load(fis);
					log.debug("Config file loaded !!!");

				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			myGmailUtility = new MyGmailUtility(emailDATA);

			BrowserFactory browserFactory = new BrowserFactory(log, config);
			WebDriver driver = browserFactory.getMyBrowser(myBrowser);
			this.context = setContext(iTestContext, driver); // setting the driver into context

			// Browser management Customization
			WebsiteTestingURL = config.getProperty("testSiteUrl");
			driver.get(WebsiteTestingURL);
			driver.manage().window().maximize();
			log.debug("Navigated to : " + config.getProperty("testSiteUrl"));
			driver.manage().timeouts().pageLoadTimeout(Integer.parseInt(config.getProperty("pageLoadTimeout")),
					TimeUnit.SECONDS);

			homePage = new HomePage(driver, log, pageDATA, 10);
			loginPage = new LoginPage(driver, log, pageDATA, 10);
			myAccountPage = new MyAccountPage(driver, log, pageDATA, 10);

		}

	}

	public static String getGmailAccount(String data) {
		if (data.equalsIgnoreCase(MyConstants.ValidGmailKeyWord)) {
			String GmailAddr = myGmailUtility.getRandomGmailAddress();
			System.setProperty("SameVaidGmail", GmailAddr);
			return GmailAddr;
		} else if (data.equalsIgnoreCase(MyConstants.sameValidGmailKeyWord)) {
			return System.getProperty("SameVaidGmail");
		} else
			return data;
	}

	public static ITestContext setContext(ITestContext iTestContext, WebDriver driver) {
		iTestContext.setAttribute("driver", driver);
		return iTestContext;
	}

	@AfterClass
	public void tearDown(ITestContext result) {
		WebDriver driver = (WebDriver) result.getAttribute("driver");
		if (driver != null)
			driver.quit();
		log.debug("Test Execution Compleated !!!");

		try {
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			fos = new FileOutputStream(System.getProperty("user.dir") + MyConstants.emailDATA_FilePath);
			try {
				emailDATA.store(fos, null);
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
