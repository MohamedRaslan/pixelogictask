package com.phptravels.listeners;

import java.io.IOException;
import java.util.Hashtable;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.phptravels.TestUtilities.MyConstants;
import com.phptravels.TestUtilities.TestUtil;
import com.phptravels.base.TestBase;

public class CustomListeners extends TestBase implements ITestListener {
	// With HashTable
	public static String resultAttributeNames;
	public static Hashtable<String, String> resultAttributeName;
	// Without HashTable
	// public static String resultAttributeNames;
	// public static String[] resultAttributeName;
	public String messageBody;

	@SuppressWarnings("unchecked")
	public void onTestStart(ITestResult result) {

		// With HashTable
		if (result.getParameters().length != 0) {
			resultAttributeName = (Hashtable<String, String>) result.getParameters()[0];
		} else
			resultAttributeName = new Hashtable<String, String>();

		resultAttributeNames = resultAttributeName.toString();

		if (TestUtil.isTestRunnable(result.getName(), MyConstants.testSuiteToRun, excel)) {

			if (result.getParameters().length != 0) {

				RepoNode = RepoTest.createNode("Testing the " + result.getName()
						+ " <br>With The Following Parameters:: <br> " + resultAttributeNames + " <br>");

				if (resultAttributeName.get("runMode").equalsIgnoreCase("Y"))
					RepoNode.info("Runnig the Test :: " + result.getName()
							+ "<br> With The Following Parameters:: <br> " + resultAttributeNames + " <br>");

				else {

					RepoNode.skip(
							"Skipping the Test :: " + result.getName() + "<br> With The Following Parameters:: <br> "
									+ resultAttributeNames + " <br> as the Run mode is No <br>");
					throw new SkipException(
							"Skipping the Test :: " + result.getName() + "\n With The Following Parameters:: \n "
									+ resultAttributeNames + " \n as the Run mode is No \n");
				}

			} else
				RepoNode = RepoTest.createNode("Testing the " + result.getName() + " <br>");

		} else {

			RepoTest.skip("Skipping the Test :: " + result.getName() + "<br> With The Following Parameters:: <br> "
					+ resultAttributeNames + " <br> as the Run mode is No <br>");
			throw new SkipException("Skipping the Test :: " + result.getName()
					+ "\n With The Following Parameters:: \n " + resultAttributeNames + " \n as the Run mode is No \n");

		}

		// runmodes - Y
	}

	public void onTestSuccess(ITestResult result) {
		RepoNode.log(Status.PASS, "The " + result.getName() + " Test <br>With The Following Parameters:: <br> "
				+ resultAttributeNames + " <br> PASSED<br>");

	}

	public void onTestFailure(ITestResult result) {
		  WebDriver driver = (WebDriver) result.getTestContext().getAttribute("driver");  // here we are accessing the driver object that we added in Test class  
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		try {
			TestUtil.captureScreenshot(driver);
			
		} catch (IOException e) {
			e.printStackTrace();
		/*	
		}catch(NullPointerException e)
		{
			e.printStackTrace();
		}*/
		}
		try {
			RepoNode.fail(
					"The " + result.getName() + " Test <br> With The Following Parameters:: <br> "
							+ resultAttributeNames + " <br> FAILD <br>" + "1. The Test Exception :: <br>"
							+ result.getThrowable() + "<br>2.The ScreenShot :: <br><br>",
					MediaEntityBuilder.createScreenCaptureFromPath(TestUtil.screenshotPath + TestUtil.screenshotName)
							.build());

		} catch (IOException XExep) {
			XExep.printStackTrace();
			RepoNode.log(Status.FAIL,
					"The " + result.getName() + "Test <br> With The Following Parameters:: <br> " + resultAttributeNames
							+ "<br> FAILD <br>" + "1. The Test Exception :: <br>" + result.getThrowable()
							+ "<br>2.The ScreenShot :: <br>" + "Loading The Image: " + TestUtil.screenshotName
							+ " Failed With Exception :: <br>" + XExep.getMessage() + " <br>");
		}

		Reporter.log("Capturing Screenshots... ");
		Reporter.log("<a target=\"_blank\" href=\"" + TestUtil.screenshotPath + TestUtil.screenshotName + "\"> "
				+ TestUtil.screenshotName + " </a>");
		Reporter.log("<br>");
		Reporter.log("<br>");
		Reporter.log("<a target=\"_blank\" href=\"" + TestUtil.screenshotPath + TestUtil.screenshotName
				+ "\"><img src=\"" + TestUtil.screenshotPath + TestUtil.screenshotName
				+ "\" height=200 width=200></img> " + TestUtil.screenshotName + " </a>");

	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onStart(ITestContext context) {

		RepoTest = Repo.createTest(context.getName());
		RepoTest.log(Status.INFO, context.getName() + " Is Starting ... <br>");

	}

	public void onFinish(ITestContext context) {
		Repo.flush();
	}

}
