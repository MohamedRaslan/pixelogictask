package forTest;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.phptravels.pom.HomePage;
import com.phptravels.pom.LoginPage;
import com.phptravels.pom.MyAccountPage;
import com.phptravels.utilities.BrowserFactory;

public class testElements {

	public static Properties config = new Properties();
	public static Properties pageDATA = new Properties();
	public static FileInputStream fis;
	public static Logger log = Logger.getLogger("devpinoyLogger");
 
	public static void main(String[] args) throws Exception {

		fis = new FileInputStream(
				System.getProperty("user.dir") + "\\src\\test\\resources\\properties\\Config.properties");

		config.load(fis);
		log.debug("Config file loaded !!!");
		fis = new FileInputStream(
				System.getProperty("user.dir") + "\\src\\test\\resources\\properties\\pageDATA.properties");

		pageDATA.load(fis);
		log.debug("pageDATA file loaded !!!");
		BrowserFactory browserFactory = new BrowserFactory(log, config);
		WebDriver driver = browserFactory.getMyBrowser("frefox");

		driver.get(config.getProperty("testSiteUrl"));
		driver.manage().window().maximize();

		HomePage homePage = new HomePage(driver, log, pageDATA, 10);
		LoginPage loginPage = new LoginPage(driver, log, pageDATA, 10);
		MyAccountPage myAccountPage = new MyAccountPage(driver, log, pageDATA, 10);

		System.out.println(homePage.accountName());

		System.out.println("IS homePage : " + homePage.isTheHomePage());

		homePage.openRegisterForm();
		System.out.println("IS loginPage : " + loginPage.isTheLoginPage());

		loginPage.RegisterNewAccount("", "", "", "", "", "", true);

		List<String> msg = loginPage.getAlertMessages();
		for (String amsg : msg)
			System.out.println(amsg);
	
		System.out.println("IS loginPage : " + loginPage.isTheLoginPage());

		loginPage.RegisterNewAccount("fdfdg", "", "fdgfdgd", "", "", "", true);

		msg = loginPage.getAlertMessages();
		for (String amsg : msg)
			System.out.println(amsg);

		loginPage.RegisterNewAccount("firstName", "lastName", "phone", "gfdg@bfgjysdsdsde.redc", "Password", "Password", true);
		System.out.println("IS myAccountPage : " + myAccountPage.isMyAccountPage());

		System.out.println("This isMyProfile : " + myAccountPage.isMyProfile("firstName", "lastName", "gfdg@bfgjysdsdsde.redc"));

		// driver.close();

	}

}
