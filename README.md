# PixelogicMedia Task

## Table of Contents
* [About the Project](#markdown-header-about-the-project)
* [Prerequisites](#markdown-header-prerequisites)
* [Quick Start](#markdown-header-quick-start)
* [Built With](#markdown-header-built-with)
* [Installations](#markdown-header-installations)
* [Getting Started Using Eclipse IDE](#markdown-header-getting-started-using-eclipse-IDE)
* [Usage](#markdown-header-usage)
* [Notes](#markdown-header-notes)


## About The Project
This is an automation framework to test the [PHPTravels Regester Form](https://www.phptravels.net/register) 
based on the requirements given in the Pixelogic Media task.
 
In this project I made a data driven framework using excel sheet with the ability to skip tests by changing the `runMode` column from Y to N for the whole test case inside a sheet called `PHPTravelsTest_suite` in the excel file `src\test\resources\excel\testdata.xlsx`  or for certain row inside " each sheet that represent its test case" .

## Prerequisites
Before you start executing the script you need to make sure form the following :

* You need to have Java and Maven already installed in your machine
* Selenuim Webdrivers are in this dirctory `src\test\resources\executables\drivers` 
    for **"Chrome version 79 & Firefox ≥ 60"** and the following Operating Systems **"Windows - Mac - Linux"**, 
    So you need to make sure that your browser match those requirements or if you faced any problem related to selenium webdriver
    you could download the suitable webdriver for your **"Chrome or Firefox "** browser and put them in the previously mentioned directory .


## Quick Start
* Clone or Download the project
* Open the CMD and type the following command
```sh
$ mvn clean test
```

    


> **Note That ::**
>> The build will fail with the following error message

>> `[ERROR] Failed to execute goal org.apache.maven.plugins:maven-surefire-plugin: 2.19.1:test (default-test)`

>> And that is fine as there is some tests didn't pass because the website does not match the given requirements &  [my assumptions](https://docs.google.com/document/d/1KbwGdEu5zPSo4Yq55C_7mAsBhR4ET7M7QHYbYaT-IyU/edit) in this task.

* Go to `target\surefire-reports\html\ExtentReports` directory and open the `ExtentRepo.html` to view the execution report

> This report is made using the [Extent Report](https://extentreports.com/) (A Customizable HTML Reporting Framework)

* You can also view the logs file from the following directory `src\test\resources\logs`

> Those log file are made using the [Apache Log4j](https://logging.apache.org/log4j/2.x/) (A Logging Utility for JAVA)


## Built With
The Framework built with the below main packages

* [Maven](https://maven.apache.org/) (Build Automation Tool)
* [TestNG](https://testng.org/doc/) (Testing Framework)
* [Selenium](https://selenium.dev/) (Web Automation UI Framework)
* [Apache POI](https://poi.apache.org/) (Read/Write From Microsoft Office formats)
* [Apache Log4j](https://logging.apache.org/log4j/2.x/) (Logging Utility for Java)
* [Extent Report](https://extentreports.com/) (Customizable HTML Reporting Framework)
* [EmailUtils.Java file by Angie Jones](http://angiejones.tech/test-automation-to-verify-email/) (Use a JavaMail API to automate the email 
verification)

## Installations
You need to install and setup the following tools

* Install [Eclipse](eclipse.org/downloads/) IDE
* Install [Java](https://www.oracle.com/technetwork/java/javase/downloads/index.html)  
* Install [Maven](http://maven.apache.org/download.cgi) 
* Setup Java & Maven environment variables
* Install Chrome and Firefox Browser

## Getting Started Using Eclipse IDE

* Clone or download the project to your local machine
* Import the project to your Eclipse IDE as Existing Maven Project.
* Run the `testng.xml` you will find it inside the following directory `src\test\resources\xmlRunners\`


> Here is how it works

>> By Running the `testng.xml` file

>>> * The test cases in `testng.xml` will run and also the listenersin the `src\test\java\com\phptravels\listeners\CustomListeners.java` file will be triggered by the following addition in the `testng.xml` file

>>>  ```xml
<listeners>
	<listener class-name="org.uncommons.reportng.HTMLReporter" />
	<listener class-name="org.uncommons.reportng.JUnitXMLReporter" />
	<listener class-name="com.phptravels.listeners.CustomListeners" />
</listeners>
 ```

>>> * Because every test case has a data provider,The data provider will read two things from the excel file `src\test\resources\excel\testdata.xlsx` twice for each test  

>>>> * One thing to see if the program should skip this test case or not by getting the data of the  `runMode` column  inside a sheet called `PHPTravelsTest_suite` in the excel file `src\test\resources\excel\testdata.xlsx` or for certain row inside each sheet that represent its test case .
>>>> * The other thing is the data of certain row inside "each sheet that represent its test case" , to give it to the test script.

## Usage

### My Project structure

```
~~~
PixeLogicTask:/
|   .gitignore
|   pom.xml
|   README.md
|
\---src
    +---main 
    |   +---java
    |   |    \----com.phptravels
    |   |          +--- \pom "Contain my Page Object Model of the website "
    |   |          |   HomePage.java
    |   |          |   LoginPage.java
    |   |          |   MyAccountPage.java
    |   |          |   PomBase.java
    |   |          | 
    |   |          \--- \utilities
    |   |                BrowserFactory.java
    |   |                EmailUtils.java
    |   |                MyGmailUtility.java
    |   |   
    |   \---resources
    \---test
        +---java
        |   \---com\phptravels\base
        |       |   TestBase.java "This is the Start Point in testing"
        |       |
        |       +---com\phptravels\listeners
        |       |       CustomListeners.java
        |       |
        |       +---com\phptravels\testCases "Contain my test script "
        |       |       VerifyEmail.java
        |       |       VerifyFirstName.java
        |       |       VerifyLastName.java
        |       |       VerifyMobileNumber.java
        |       |       VerifyPassAndConfirmPass.java
        |       |       VerifyUserCanCreateAccount.java
        |       |
        |       +---com\phptravels\TestUtilities
        |       |       ExcelReader.java
        |       |       ExtentRepoManager.java
        |       |       ExtentRepoManager.java
        |       |       TestUtil.java
        |       |
        |       \---\forTest "Contain unimportant java class i used to test my POM before making the test cases"
        |               
        |
        \---resources
            +--- \excel
            |    testdata.xlsx "This is the excel sheet for the data driven framework"
            |
            +--- \executables
            |         \---\drivers "Selenium Webdrivers are here"
            |               +--- ...
            |               |
            |               |
            |               +--- ...
            |
            +--- \logs "Log files are here"
            |
            |
            +--- \properties "Contain Configuration files"
            |
            |
            +--- \reportConfig
            |        ReportsConfig.xml "Configuration file for the [Extent Report](https://extentreports.com/)"
            |
            |
            |
            \---xmlRunners
                  testng.xml
~~~
```

* Ues the BrowserFactory class to dynamically build an enhanced webdriver per instance.
* Triggers Test Case scripts using the TestNG framework.
* The files in the com.phptravels.pom folder are used for storing DOM locator references. These references are shared by multiple scripts and means maintenance is centralised. My




## Notes

* Open this file [TestPlanFor_PHPTravelsTask](https://docs.google.com/document/d/159UYbxaddocm-NPaFwM0LdYq-0BZ3LqU) to view all the supplied deliverables
* I will update this Readme file soon , because unfortunately it was not completed as I didn't spend much time on it 